package com.pligor.myplay

import play.api.mvc.{EssentialAction, Controller}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait GetRealIpActionTrait {
  this: Controller =>

  protected def GetRealIpAction(action: RealIp => EssentialAction): EssentialAction = {
    EssentialAction {
      request =>
        action(RealIp(request))(request)
    }
  }

}
