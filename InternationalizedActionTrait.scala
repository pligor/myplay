package com.pligor.myplay

import play.api.mvc.{RequestHeader, EssentialAction, Controller}
import com.pligor.generic.HttpHeaderName

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait InternationalizedActionTrait extends Controller {
  protected def InternationalizedAction(action: MyLang => EssentialAction): EssentialAction = {
    def getContentLanguage(request: RequestHeader): String = {
      request.headers.get(HttpHeaderName.CONTENT_LANGUAGE.toString).getOrElse(M.defaultLang)
    }

    /*def getLang(contentLanguage: String) = {
      Lang.get(
        contentLanguage.take(2)
      ).getOrElse(Lang(myplay.M.defaultLang.take(2)))
    }*/

    EssentialAction {
      request =>
        val contentLanguage = getContentLanguage(request)

        val myLang = MyLang.parse(contentLanguage)

        action(myLang)(request)
    }
  }
}
