package com.pligor.myplay

import play.api.i18n.Messages

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object M {
  val defaultLang = "en_GB"

  def apply(key: String, args: Any*)(implicit lang: MyLang): String = Messages(key, args: _ *)(lang.lang)

  /*def apply(key: String, args: Any*)(implicit lang: Lang): String = {
    if (lang.language == defaultLang) {
      Messages(key, args);
    }
    else {
      Messages(key.replace(' ', '_'), args)
    }
  }*/
}
