package com.pligor.myplay

import play.api.mvc.RequestHeader
import com.codahale.metrics.{MetricFilter, MetricRegistry}
import com.codahale.metrics.graphite.{GraphiteReporter, Graphite}
import java.net.InetSocketAddress
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MetricHelper {
  private val slash = '/'

  private val emptyPathName = "index"

  private val statsdServerPort = 2003

  def findRouteWithMethod(prefix: String, request: RequestHeader, routesPrefixes: Seq[String]) = {
    findRoute(prefix, request.path, routesPrefixes).map(
      routeFound =>
        MetricRegistry.name(routeFound, request.method.toLowerCase)
    )
  }

  private def findRoute(prefix: String, requestPath: String, routesPrefixes: Seq[String]) = {
    (if (requestPath == slash.toString) {
      Some(emptyPathName)
    } else {
      (routesPrefixes find requestPath.startsWith).map(_.tail)
    }).map(routePrefix => composeMetric(prefix, splitRoute(normalizeRoute(routePrefix))))
  }

  private def composeMetric(prefix: String, routeParts: Seq[String]) = {
    MetricRegistry.name(prefix, routeParts: _ *)
  }

  private def splitRoute(route: String) = {
    route.split(slash)
  }

  private def normalizeRoute(route: String) = {
    route.filter(c => c.isLetterOrDigit || c == slash)
  }

  def startAndGetGraphiteReporter(socketHostName: String,
                            apiKey: String,
                            metricRegistry: MetricRegistry,
                            period: Duration) = {
    val graphite = new Graphite(new InetSocketAddress(socketHostName, statsdServerPort))

    val graphiteReporter = GraphiteReporter.forRegistry(metricRegistry)
      .prefixedWith(apiKey)
      .convertRatesTo(TimeUnit.SECONDS)
      .convertDurationsTo(TimeUnit.MILLISECONDS)
      .filter(MetricFilter.ALL)
      .build(graphite)

    graphiteReporter.start(period.toMinutes, TimeUnit.MINUTES)

    graphiteReporter
  }
}
