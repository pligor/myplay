package com.pligor.myplay

import play.api.{Logger, Application, GlobalSettings}
import com.codahale.metrics.MetricRegistry
import play.api.mvc.{Handler, RequestHeader}
import com.pligor.myplay.MetricHelper._
import com.codahale.metrics.graphite.GraphiteReporter
import scala.concurrent.duration.Duration

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * http://metrics.codahale.com/getting-started/
 */
trait MetricsGlobally extends GlobalSettings {

  //abstract

  protected def getMetricsEnabled(app: Application): Boolean

  protected def metricSocketHostName: String

  protected def metricsApiKey: String

  protected def metricsGlobalPrefix: String

  protected def metricsReportPeriod: Duration

  protected def routesPrefixes: Seq[String]

  //concrete

  private def metricRoutesPrefix = MetricRegistry.name(metricsGlobalPrefix, "routes")

  private var metrics: Option[MetricRegistry] = None

  private var graphiteReporter: Option[GraphiteReporter] = None

  private var isMetricsEnabled: Boolean = false

  override def onStart(app: Application): Unit = {
    super.onStart(app)

    isMetricsEnabled = getMetricsEnabled(app)

    if(isMetricsEnabled) {
      Logger debug "go metrics"

      metrics = Some(new MetricRegistry)

      graphiteReporter = Some(
        startAndGetGraphiteReporter(
          socketHostName = metricSocketHostName,
          apiKey = metricsApiKey,
          metricRegistry = metrics.get,
          period = metricsReportPeriod
        )
      )
    } else {
      //nop
    }
  }

  override def onStop(app: Application): Unit = {
    super.onStop(app)

    Logger debug "stop metrics"

    graphiteReporter.map(_.stop())

    graphiteReporter = None

    metrics = None

    isMetricsEnabled = false
  }

  /**
   * THIS SOLUTION MIGHT WORK TO GET ROUTES:
   * http://blog.jierenchen.net/2012/04/routes-lookup-in-play-framework-20.html
   * @return
   */
  override def onRouteRequest(request: RequestHeader): Option[Handler] = {
    if(isMetricsEnabled) {
      tryMeterRequest(request)
    } else {
      //nop
    }

    super.onRouteRequest(request)
  }

  private def tryMeterRequest(request: RequestHeader) = {
    val metricKeyOption = findRouteWithMethod(
      metricRoutesPrefix,
      request,
      routesPrefixes
    )

    //Logger debug s"request path: ${request.path}"

    if (metricKeyOption.isDefined) {
      val requestMetricKey = metricKeyOption.get

      //they are metrics, they should NOT crash our system!

      metrics.map(_.meter(requestMetricKey).mark())

      /*
      a count is included in the mark
      val counter = metrics.get.counter(requestMetricKey)
      counter.inc()*/

      Logger debug s"requestMetricKey: $requestMetricKey"
    } else {
      //no metrics
    }
  }
}
