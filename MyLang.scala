package com.pligor.myplay

import play.api.i18n.Lang
import java.util.Locale

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MyLang {
  val defaultCountryCode = Locale.US.getCountry

  //val default = MyLang(Lang(myplay.M.defaultLang), defaultCountryCode)

  def parse(str: String): MyLang = {

    def getWithDefaultCountryCode = {
      MyLang(Lang(str.take(2), defaultCountryCode))
    }

    val sep = '_'

    if (str.contains(sep.toString)) {
      val array = str.split(sep)

      if (array.length == 2) {
        MyLang(Lang(array.head, array.last))
      } else {
        getWithDefaultCountryCode
      }
    } else {
      getWithDefaultCountryCode
    }
  }
}

case class MyLang(lang: Lang) {

}
