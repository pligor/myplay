package com.pligor.myplay

import java.io.File
import play.api.Application
import scala.concurrent.ExecutionContext
import play.api.libs.ws.WS
import play.api.libs.iteratee.Enumerator
import play.api.mvc._
import com.pligor.generic.{ContentType, HttpHeaderName}
import play.api.mvc.ResponseHeader
import play.api.mvc.SimpleResult

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object PlayHelper {
  //just to cache it
  private val s = File.separator;

  def getAbsoluteLinkPrefix(hostName: String)(implicit application: Application) = {
    application.configuration.getString("transferProtocol").get + "://" + hostName
  }

  def getPlayPath(implicit application: Application) = application.path.getCanonicalPath;

  private class DebuggableConfigurationParameterMissingException extends Exception;

  def isDebuggable(implicit application: Application) = {
    application.configuration.getBoolean("debuggable").getOrElse(
      throw new DebuggableConfigurationParameterMissingException
    );
  }

  /**
   * Always enclose it in an async
   * @param preKnownContentLength specify this otherwise play has to save entire content in memory (not good!)
   * @return
   */
  def urlMethodGetAndStream(url: String, preKnownContentLength: Option[Long])
                           (implicit execContext: ExecutionContext) = {
    WS.url(url).get().map {
      response =>
        val enum: Enumerator[Array[Byte]] = Enumerator.fromStream(response.getAHCResponse.getResponseBodyAsStream)

        SimpleResult(
          header = preKnownContentLength.map(len => ResponseHeader(200, Map(HttpHeaderName.CONTENT_LENGTH.toString -> len.toString))).getOrElse(ResponseHeader(200)),
          body = enum
        ).as(ContentType.JPEG.toString)
    }
  }

  def requestToString(request: Request[AnyContent]): String = {
    val string = StringBuilder.newBuilder

    string ++= "request path: " + request.path + "\n<br/>\n"

    string ++= "request uri: " + request.uri + "\n<br/>\n"

    string ++= "request rawQueryString: " + request.rawQueryString + "\n<br/>\n"

    string ++= "request query string: " + request.queryString + "\n<br/>\n"

    string ++= "request remote address: " + request.remoteAddress + "\n<br/>\n"

    string ++= "request method: " + request.method + "\n<br/>\n"

    string ++= "request headers: " + request.headers + "\n<br/>\n"

    string ++= "request tags: " + request.tags + "\n<br/>\n"

    string ++= "request contentType: " + request.contentType + "\n<br/>\n"

    string ++= "request charset: " + request.charset + "\n<br/>\n"

    string ++= "request domain: " + request.domain + "\n<br/>\n"

    string ++= "request body as text: " + request.body.asText + "\n<br/>\n"

    string ++= "request body as json: " + request.body.asJson + "\n<br/>\n"

    string ++= "request body as form url encoded: " + request.body.asFormUrlEncoded + "\n<br/>\n"

    string ++= "request body as xml: " + request.body.asXml + "\n<br/>\n"

    string.toString()
  }
}
