What you will find inside:

* Some Interesting Action Wrappers
* A helper class for messages and intenationalization
* A play helper object

###Dependencies###
* Play 2
* com.pligor.generic
* com.codahale.metrics
* org.apache.commons.validator

Used in the following projects:

* [bman](http://bman.co)
* [Fcarrier](http://facebook.com/FcarrierApp)