package com.pligor.myplay

import play.api.mvc.RequestHeader
import com.pligor.generic.HttpHeaderName
import org.apache.commons.validator.routines.InetAddressValidator

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object RealIp {
  def apply(request: RequestHeader): RealIp = new RealIp(
    directly = request.remoteAddress,
    apacheOption = request.headers.get(HttpHeaderName.X_FORWARDED_FOR.toString),
    nginxOption = request.headers.get(HttpHeaderName.X_REAL_IP.toString)
  )
}

class RealIp(val directly: String, apacheOption: Option[String], nginxOption: Option[String]) {
  private val ipValidator = InetAddressValidator.getInstance()

  private def tryGetIp(ip: String): Option[String] = {
    if (ipValidator.isValidInet4Address(ip))
      Some(ip)
    else
      None
  }

  val apache = apacheOption.flatMap(tryGetIp)

  val nginx = nginxOption.flatMap(tryGetIp)

  def compare(ip: String): Boolean = {
    ip == directly || apache.exists(_ == ip) || nginx.exists(_ == ip)
  }
}
