package com.pligor.myplay

import play.api.libs.json.{Json, JsValue}
import play.api.Play.current
import scala.slick.driver.MySQLDriver.simple._
import com.pligor.mydb.SlickHelper._
import play.api.mvc.Controller
import play.api.mvc.Request
import play.api.mvc.Result
import com.pligor.zerobill_request_response.LoginRequest

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait SocialAuthActionTrait extends Controller {
  //abstract

  protected def retrieveUserId(loginRequest: LoginRequest)(implicit session: Session): Option[Int]

  //concrete

  protected val socialCredentialsKey = "social_credentials"

  private def parseSocialCredentials(jsValue: JsValue): Option[LoginRequest] = {
    Json.fromJson(jsValue \ socialCredentialsKey)(LoginRequest.jsonFormat).asOpt
  }

  protected def workWithAuthenticatedUserOrElse[A](loginRequest: LoginRequest)(authorized: Int => A, unauthorized: => A): A = {
    val userIdOption = onDBsession(implicit session => retrieveUserId(loginRequest))

    if (userIdOption.isDefined) {
      authorized(userIdOption.get)
    } else {
      unauthorized
    }
  }

  protected def SocialAuthAction(op: (Int, LoginRequest) => Result)(implicit request: Request[JsValue]): Result = {
    val loginRequestOption = parseSocialCredentials(request.body)

    if (loginRequestOption.isDefined) {
      val loginRequest = loginRequestOption.get

      workWithAuthenticatedUserOrElse(loginRequest)(authorized = {
        userId =>
          op(userId, loginRequest)
      }, unauthorized = {
        //Logger.info("you ate a door while you were trying to authenticate");
        Unauthorized("").withHeaders("WWW-Authenticate" -> "") //this header is mandatory from protocol
      })
    } else {
      //Logger.info("bad request while trying to authenticate");
      BadRequest("")
    }
  }

}
