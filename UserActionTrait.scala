package com.pligor.myplay

import play.api.mvc._
import play.api.libs.json.JsValue
import play.api.db.DB
import play.api.Play.current
import play.api.libs.json.JsString
import java.sql.Connection

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait UserActionTrait extends Controller {
  //abstract

  protected def retrieveUserId(token: String)(implicit connection: Connection): Option[Long]

  //concrete

  protected def workWithAuthenticatedUserOrElse[A](token: String)(authorized: (Long) => A, unauthorized: => A): A = {
    val userIdOption = DB.withConnection(implicit connection => retrieveUserId(token))
    if (userIdOption.isDefined) {
      authorized(userIdOption.get)
    } else {
      unauthorized
    }
  }

  protected def UserAction(op: Long => (Request[JsValue] => Result)) = {
    Action(parse.json) {
      implicit request =>
        val tokenOption = request.body \ "token" match {
          case JsString(token) => Some(token)
          case _ => None
        }

        if (tokenOption.isDefined) {
          workWithAuthenticatedUserOrElse(tokenOption.get)(authorized = {
            userId =>
              op(userId)(request)
          }, unauthorized = {
            //Logger.info("you ate a door while you were trying to authenticate");
            Unauthorized("").withHeaders("WWW-Authenticate" -> ""); //this header is mandatory from protocol
          })
        } else {
          //Logger.info("bad request while trying to authenticate");
          BadRequest("")
        }
    }
  }
}

