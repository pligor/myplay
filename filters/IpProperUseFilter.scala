package com.pligor.myplay.filters

import play.api.mvc.{SimpleResult, Result, RequestHeader, Filter}
import play.api.Logger
import com.pligor.myplay.RealIp
import java.net.InetAddress
import scala.concurrent.duration._
import play.api.db.DB
import play.api.Application
import java.sql.Connection
import scala.concurrent._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class IpProperUseFilter(shortDuration: Duration, maxPerShortDuration: Int,
                        longDuration: Duration, maxPerLongDuration: Long)
                       (implicit app: Application, execCtx: ExecutionContext)
  extends Filter with play.api.mvc.Results {

  protected def insertRequest(inetAddress: InetAddress)(implicit conn: Connection): Option[Long]

  protected def countRequests(inetAddress: InetAddress, duration: Duration)(implicit conn: Connection): Long

  def apply(next: (RequestHeader) => Result)(rh: RequestHeader): Result = {
    val realIp = RealIp.apply(rh)

    val ipAddress = realIp.nginx.getOrElse(realIp.apache.getOrElse(realIp.directly))

    val inetAddress = InetAddress.getByName(ipAddress)

    DB.withConnection {
      implicit conn =>

        val newIpId = insertRequest(inetAddress)

        if (countRequests(inetAddress, shortDuration) > maxPerShortDuration
          || countRequests(inetAddress, longDuration) > maxPerLongDuration) {
          Logger debug "inside ip filter"

          //future {
            TooManyRequest
          //}
        } else {
          next(rh)
        }
    }
  }
}


/*object DummyFilter extends Filter {
  def apply(next: (RequestHeader) => Result)(rh: RequestHeader): Result = {

    Logger debug "mesa sto dummy"

    next(rh)
  }
}*/
